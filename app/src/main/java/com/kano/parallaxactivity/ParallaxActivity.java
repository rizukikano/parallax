package com.kano.parallaxactivity;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;

import com.kano.parallaxactivity.ParallaxView;

public class ParallaxActivity extends Activity {

    private ParallaxView parallaxView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Display display = getWindowManager().getDefaultDisplay();

        Point resolution = new Point();
        display.getSize(resolution);

        parallaxView = new ParallaxView(this, resolution.x, resolution.y);

        setContentView(parallaxView);

    }

    @Override
    protected void onPause() {
        super.onPause();
        parallaxView.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        parallaxView.resume();
    }
}
